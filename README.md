Instructions for working with this project. 

For System Engineers: 
   Create branches of the report for your projects. You will be ediiting files 
   report.tex (but there's very little), intro.tex, SystemsEngineering.tex. 
   
For Team Members. 
  1. Make sure you check out the branch that corresponds to your project. 
  2. You just need to edit one file that corresponds to your subsystem. Store 
     all of your images in the images directory. 
  3. Before submitting your work, please make sure that your work compiles 
     without errors. Otherwise, your colleagues will not be able to compile 
     properly. 
    
Resources for GIT. 
   SourceTree is a nice software that takes guessing work out of GIT. 
   Just make sure you're working with the proper branch. 
   
   Typical workflow: 
      Summarized here: https://hikaruzone.files.wordpress.com/2015/10/in-case-of-fire-1-git-commit-2-git-push-3-leave-building2.png?w=800&h=559
      
    Start working (name of the branch will be given by SysEng) 
       git clone --branch MySat 
       
    Further work 
       After Editing file: git commit Subsystem.tex 
       When Done with working on file: git push Subsystem.tex 
       
Latex resources 
  Typicaly just google it. Many resources are available. 